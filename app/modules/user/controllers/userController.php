<?php
/**
 * Class for User Controller
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */

class userController extends controllerGrid {

    protected $restrictController = true;

    private $userModel;

    public function init() {
        parent::init();

        $this->userModel = new userModel();

        $this->setTitle('Controle de Usuários');
        $this->setFooter('footer');
        $this->setHeader('header');
    }

    public function indexAction() {

        $dados = array();

        $dados['list'] = $this->userModel->find()->getArrayResult();

        $grid = new Grid('user');
        $grid->setColumn('ID', 'entity_id')
            ->setColumn('Usuário', 'user', "class='center'")
            ->setColumn('Senha', 'password', "class='center'")
            ->setList($dados['list'])
            ->setEditAction('/user/user/edit')
            ->setDeleteAction('/user/user/delete')
            ->setPkName('entity_id')
            ->setTitle('Lista de Usuários');
        $grid = $grid->getGrid();

        $dados['grid'] = $grid;

        $this->loadTemplate('grid', $dados);

    }

    public function userpostAction() {

        if ($this->getParams('password') == '') {
            self::addMessageError('A senha deve ser preenchida');
            $this->goToUrl('/user/user/index');
            exit;
        }

        if ($this->getParams('user') == '') {
            self::addMessageError('O usuário deve ser preenchido');
            $this->goToUrl('/user/user/index');
            exit;
        }

        $this->userModel->setValue('user', "{$this->getParams('user')}");
        $this->userModel->setValue('password', md5($this->getParams('password')));

        try {
            $this->userModel->insert();
        } catch(Exception $e) {
            self::addMessageError($e->getMessage());
            $this->goToUrl('/user/user/index');
            exit;
        }

        self::addMessageSuccess('Usuário cadastrado com sucesso.');

        $this->goToUrl('/user/user/index');

    }

    public function editAction() {

        $result = $this->userModel->loadById($this->getParams('id'), 'entity_id')->getArrayResult();
        $dados['values'] = $result[0];

        $this->loadTemplate('edit', $dados);
    }

    public function editpostAction() {

        try {

            if ($this->getParams('id') == '') {
                self::addMessageError('O parâmetro id deve ser informado');
                $this->goToUrl('/user/user/index'.$this->getParams('id'));
                exit;
            }

            if ($this->getParams('user') == '') {
                self::addMessageError('Usuário deve ser preenchido');
                $this->goToUrl('/user/user/edit/id/'.$this->getParams('id'));
                exit;
            }

            $this->userModel->setValue('user', $this->getParams('user'));
            if ($this->getParams('password') != '') {
                $this->userModel->setValue('password', md5($this->getParams('password')));
            }
            $this->userModel->update($this->getParams('id'));

            self::addMessageSuccess('Usuário atualizado com sucesso');

            $this->goToUrl('/user/user/index');

        } catch (Exception $e) {
            self::logException($e->getMessage());
            self::addMessageError('Erro ao atualizar o usuário ' . $this->getParams('user'));
            $this->goToUrl('/user/user/edit/id/'.$this->getParams('id'));
        }

    }

    public function deleteAction() {

        try {

            if ($this->getParams('id') == '') {
                self::addMessageError('O parâmetro id deve ser informado');
                $this->goToUrl('/user/user/index'.$this->getParams('id'));
                exit;
            }

            $this->userModel->delete($this->getParams('id'));

            self::addMessageSuccess('Usuário deletado com sucesso');

            $this->goToUrl('/user/user/index');

        } catch (Exception $e) {
            self::logException($e->getMessage());
            self::addMessageError('Erro ao deletar o usuário ' . $this->getParams('user'));
            $this->goToUrl('/user/user/index');
        }
    }
}