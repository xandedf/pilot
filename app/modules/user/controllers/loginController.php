<?php
/**
 * Class for Login Controller
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */

class loginController extends Pilot_Core_Controller {

    private $userModel;

    public function init() {
        parent::init();

        $this->userModel = new userModel();

        $this->setTitle('Login de Usuário');
        $this->setFooter('footerLogin');
        $this->setHeader('header');
    }

    public function indexAction() {

        $this->loadTemplate('login');

    }

    public function loginpostAction() {

        if ($this->getParams('user') == '') {
            self::addMessageError('O usuário deve ser preenchido');
            $this->goToUrl('/user/login');
            exit;
        }

        if ($this->getParams('password') == '') {
            self::addMessageError('A senha deve ser preenchida');
            $this->goToUrl('/user/login');
            exit;
        }

        try {
            $this->userModel->authenticate($this->getParams('user'), $this->getParams('password'));
            $this->goToUrl('/system/index');
        } catch (Exception $e) {
            self::addMessageError($e->getMessage());
            $this->goToUrl('/user/login');
            exit;
        }

    }

    public function logoutAction() {

        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
        $this->goToUrl('/user/login');

    }

}