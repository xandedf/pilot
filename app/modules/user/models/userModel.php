<?php
/**
 * Class for Customer Model
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */

class userModel extends Pilot_Core_Model {
    
    protected $table_name = 'user_entity';
    protected $id = 'entity_id';

    /**
     * Método de autenticação
     *
     * @param $username
     * @param $password
     * @return bool
     * @throws Exception
     */
    public function authenticate($username, $password) {

        try {
            $this->loadById("'{$username}'", 'user');

            if ($this->getCountResult() == 0) {
                throw new Exception('Usuário não reconhecido');
            }

            if ($this->getValue('password') === md5($password)) {
                $values = $this->getArrayResult();
                $_SESSION['user'] = $values[0];
                return true;
            }

            throw new Exception('Senha inválida');

        } catch(Exception $e) {
            self::addMessageError($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    public function insert() {

        try {

            $this->loadById($this->values['user'], 'user', true);

            if ($this->getValue($this->id)) {
                throw new Exception('Usuário já existe na base');
            }

            parent::insert();

        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

}