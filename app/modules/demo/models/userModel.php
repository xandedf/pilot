<?php
/**
 * Definição do Customer Model 
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2013 Discover (http://www.discover.com.br) 
 * @license     Commercial
 */

class userModel extends Pilot_Core_Model {
    
    protected $table_name = 'user_entity';
    
}