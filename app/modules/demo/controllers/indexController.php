<?php

class indexController extends controllerGrid {
    
    public function init() {
        parent::init();
        
        $this->setTitle('Página de Exemplos');
        $this->setFooter('footer');
        $this->setHeader('header');
    }


    public function indexAction() {
        
        $user = new userModel();
        
        $dados['list'] = $user->find()->getArrayResult();
        
        $this->loadTemplate('index', $dados);
        
    }
    
    public function listAction() {
        
        $this->setCSS('list.css');
        
        $customer = new customerModel();
        
        $dados['list'] = $customer->find()->getArrayResult();
        
        $this->loadTemplate('list', $dados);
        
    }


    public function insertAction() {
        
        $user = new userModel();

        $user->setValue('name', $this->getParams('nome'));
        $user->setValue('email','teste@teste.com.br');
        $user->setValue('telefone',$this->getParams('telefone'));
        
        $dados['id'] = $user->insert()->getReturnId();
        $dados['list'] = $user->find()->getArrayResult();
        
        $this->loadTemplate('index', $dados);
        
    }
    
    public function gridAction() {
        
        $user = new userModel();
        
        $dados['list'] = $user->find()->getArrayResult();
        
        $grid = new Grid('usergrid');
        $grid->setColumn('ID', 'entity_id')
             ->setColumn('Usuário', 'user', "class='center'")
             ->setColumn('Senha', 'password', "class='center'")
             ->setList($dados['list'])
             ->setTitle('Lista de Usuários');
        $grid = $grid->getGrid();
        
        $dados['grid'] = $grid;
        
        $this->loadTemplate('grid', $dados);
        
    }
    
}
