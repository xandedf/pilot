<?php
/**
 * Class for Index Controller
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */


class indexController extends Pilot_Core_Controller {

    protected $restrictController = true;

    public function init() {
        parent::init();

        $this->setTitle('Sistema de Livros');
        $this->setFooter('footer');
        $this->setHeader('header');
    }

    public function indexAction() {

        $dados['booklist'] = $this->_getListOfBooks(10);

        $gridBook = new Grid('book');
        $gridBook->setColumn('ID', 'entity_id')
            ->setColumn('Título', 'title', "class='center'")
            ->setColumn('Editor', 'name', "class='center'")
            ->setColumn('Ano', 'year', "class='center'")
            ->setColumn('Edição', 'edition', "class='center'")
            ->setList($dados['booklist'])
            ->setTitle('Últimos Livros');
        $gridBook = $gridBook->getGrid();
        $dados['gridbooks'] = $gridBook;

        $dados['editorList'] = $this->_getLisfOfEditors(10);
        $gridEditor = new Grid('editor');
        $gridEditor->setColumn('ID', 'entity_id')
            ->setColumn('Nome', 'name', "class='center'")
            ->setList($dados['editorList'])
            ->setTitle('Últimos Editores');
        $gridEditor = $gridEditor->getGrid();
        $dados['grideditors'] = $gridEditor;

        $this->loadTemplate('system', $dados);

    }

    private function _getListOfBooks($limit) {

        $bookModel = new bookModel();
        return $bookModel->setJoin('book_entity', 'editor_id', 'editor_entity', 'editor', 'entity_id', self::type_left_join)
                        ->setLimit($limit)
                        ->find()
                        ->getArrayResult();



    }

    private function _getLisfOfEditors($limit) {

        $editorModel = new editorModel();
        return $editorModel->setLimit($limit)
                            ->find()
                            ->getArrayResult();
    }
}