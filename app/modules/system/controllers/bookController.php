<?php
/**
 * Class for Book Controller
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */

class bookController extends Pilot_Core_Controller {

    protected $restrictController = true;

    public function init() {
        parent::init();

        $this->setTitle('Sistema de Livros');
        $this->setFooter('footer');
        $this->setHeader('header');
    }

    public function listAction() {

        $dados['booklist'] = $this->_getListOfBooks();

        $gridBook = new Grid('book');
        $gridBook->setColumn('ID', 'entity_id')
            ->setColumn('Título', 'title', "class='center'")
            ->setColumn('Editor', 'name', "class='center'")
            ->setColumn('Ano', 'year', "class='center'")
            ->setColumn('Edição', 'edition', "class='center'")
            ->setList($dados['booklist'])
            ->setEditAction('/system/book/edit')
            ->setDeleteAction('/system/book/delete')
            ->setPkName('entity_id')
            ->setTitle('Listagem de Livros');
        $gridBook = $gridBook->getGrid();
        $dados['gridbooks'] = $gridBook;

        $this->loadTemplate('listBook', $dados);

    }

    private function _getListOfBooks() {

        $bookModel = new bookModel();
        return $bookModel->setJoin('book_entity', 'editor_id', 'editor_entity', 'editor', 'entity_id', self::type_left_join)
                        ->find()
                        ->getArrayResult();

    }
}