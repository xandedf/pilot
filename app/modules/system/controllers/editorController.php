<?php
/**
 * Class for Editor Controller
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */


class editorController extends Pilot_Core_Controller {

    protected $restrictController = true;

    public function init() {
        parent::init();

        $this->setTitle('Sistema de Livros');
        $this->setFooter('footer');
        $this->setHeader('header');
    }

    public function listAction() {

        $dados['editorList'] = $this->_getLisfOfEditors();
        $gridEditor = new Grid('editor');
        $gridEditor->setColumn('ID', 'entity_id')
            ->setColumn('Nome', 'name', "class='center'")
            ->setList($dados['editorList'])
            ->setEditAction('/system/editor/edit')
            ->setDeleteAction('/system/editor/delete')
            ->setPkName('entity_id')
            ->setTitle('Lista Editores');
        $gridEditor = $gridEditor->getGrid();
        $dados['grideditors'] = $gridEditor;

        $this->loadTemplate('listEditor', $dados);

    }

    private function _getLisfOfEditors() {

        $editorModel = new editorModel();
        return $editorModel->find()->getArrayResult();

    }
}