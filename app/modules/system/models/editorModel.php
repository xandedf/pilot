<?php
/**
 * Class for Editor Model
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */


class editorModel extends Pilot_Core_Model {

    protected $table_name = 'editor_entity';
    protected $id = 'entity_id';

}