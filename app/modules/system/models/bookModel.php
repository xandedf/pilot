<?php
/**
 * Class for Book Model
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */


class bookModel extends Pilot_Core_Model {

    protected $table_name = 'book_entity';
    protected $id = 'entity_id';

}