<?php

/**
 * Class for Grid Controller
 *
 * @package     Pilot
 * @copyright   Copyright (c) 2015 ARG Softwares
 * @license     Commercial
 * @author      Alexandre Gomes <xandergomes@gmail.com>
 */

class controllerGrid extends Pilot_Core_Controller {
    
    public function init() {
        parent::init();
        $this->setJSLibs('bootstrap/js/jquery.dataTables.min.js');
    }
    
}